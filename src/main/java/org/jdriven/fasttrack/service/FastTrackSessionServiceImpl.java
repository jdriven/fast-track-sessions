package org.jdriven.fasttrack.service;

import lombok.RequiredArgsConstructor;
import org.jdriven.fasttrack.model.FastTrackSession;
import org.jdriven.fasttrack.repository.FastTrackSessionRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
public class FastTrackSessionServiceImpl implements FastTrackSessionService {

    private final FastTrackSessionRepository repository;

    @Override
    public Iterable<FastTrackSession> getAllSessions() {
        return repository.findAll();
    }

    @Override
    public FastTrackSession getSessionById(Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("FastTrackSession could not be found."));
    }

    @Override
    public FastTrackSession createSession(FastTrackSession session) {
        return repository.save(session);
    }

    @Override
    public FastTrackSession updateSession(FastTrackSession session) {
        return repository.save(session);
    }

    @Override
    public void deleteSession(FastTrackSession session) {
        repository.delete(session);
    }
}
