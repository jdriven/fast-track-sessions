package org.jdriven.fasttrack.service;

import org.jdriven.fasttrack.model.FastTrackSession;

public interface FastTrackSessionService {

    Iterable<FastTrackSession> getAllSessions();

    FastTrackSession getSessionById(Long id);

    FastTrackSession createSession(FastTrackSession session);

    FastTrackSession updateSession(FastTrackSession session);

    void deleteSession(FastTrackSession session);
}
