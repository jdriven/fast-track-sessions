package org.jdriven.fasttrack.rest;

import lombok.RequiredArgsConstructor;
import org.jdriven.fasttrack.model.FastTrackSession;
import org.jdriven.fasttrack.service.FastTrackSessionService;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/sessions")
public class FastTrackSessionRestController {

    private final FastTrackSessionService service;

    @GetMapping
    public Iterable<FastTrackSession> getAllSessions() {
        return service.getAllSessions();
    }

    @GetMapping("/{id}")
    public FastTrackSession getSessionById(@PathVariable Long id) {
        return service.getSessionById(id);
    }

    @PostMapping
    public FastTrackSession createSession(@RequestBody @Valid FastTrackSession session) {
        return service.createSession(session);
    }

    @PutMapping("/{id}")
    public FastTrackSession updateSession(@PathVariable Long id, @RequestBody @Valid FastTrackSession session) {
        if (session.getId() == null || !ObjectUtils.nullSafeEquals(id, session.getId())) {
            throw new IllegalArgumentException("Id of FastTrackSession does not equal");
        }
        return service.updateSession(session);
    }

    @DeleteMapping("/{id}")
    public void deleteSession(@PathVariable Long id) {
        service.deleteSession(service.getSessionById(id));
    }
}
