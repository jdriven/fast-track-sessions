package org.jdriven.fasttrack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastTrackSessionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FastTrackSessionsApplication.class, args);
	}
}
