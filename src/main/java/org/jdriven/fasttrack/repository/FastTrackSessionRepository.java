package org.jdriven.fasttrack.repository;

import org.jdriven.fasttrack.model.FastTrackSession;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FastTrackSessionRepository extends CrudRepository<FastTrackSession, Long> {
}
