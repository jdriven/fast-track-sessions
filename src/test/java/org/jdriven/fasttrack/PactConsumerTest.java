package org.jdriven.fasttrack;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.model.RequestResponsePact;
import au.com.dius.pact.provider.junit.IgnoreNoPactsToVerify;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;

import static org.assertj.core.api.Java6Assertions.assertThat;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "FastTrackSessionProvider")
@IgnoreNoPactsToVerify
public class PactConsumerTest {

    @Pact(provider = "FastTrackSessionProvider", consumer = "test_consumer")
    public RequestResponsePact createPact(PactDslWithProvider builder) throws Exception {
        String jsonAsString = FileUtils.readFileToString(new File(PactConsumerTest.class.getResource("/sessions.json").toURI()), "UTF-8");

        return builder
                .given("A FastTrackSession list")
                .uponReceiving("The list of FastTrackSessions")
                .path("/sessions")
                .method("GET")
                .willRespondWith()
                .body(jsonAsString)
                .status(200)
                .toPact();
    }

    @Test
    void test(MockServer mockServer) throws Exception {
        HttpResponse httpResponse = Request.Get(mockServer.getUrl() + "/sessions").execute().returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);
    }
}
