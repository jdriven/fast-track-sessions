package org.jdriven.fasttrack.service;

import org.jdriven.fasttrack.model.FastTrackSession;
import org.jdriven.fasttrack.repository.FastTrackSessionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class FastTrackSessionServiceIntTest {

    @Autowired
    private FastTrackSessionService service;

    @MockBean
    private FastTrackSessionRepository repository;

    @Test
    void testGetAll() {
        FastTrackSession session = FastTrackSession.builder().id(1L).subject("Spring Boot 2").trainer("Willem Cheizoo")
                .startTime(LocalDateTime.now().minus(Duration.ofHours(1)))
                .endTime(LocalDateTime.now().plus(Duration.ofHours(1)))
                .build();

        when(repository.findAll()).thenReturn(Collections.singletonList(session));

        Iterable<FastTrackSession> sessions = service.getAllSessions();
        assertThat(sessions).containsOnly(session);
    }

    @Test
    void testGetAll2() {
        FastTrackSession session = FastTrackSession.builder().id(1L).subject("Spring Boot 2").trainer("Willem Cheizoo")
                .startTime(LocalDateTime.now().minus(Duration.ofHours(1)))
                .endTime(LocalDateTime.now().plus(Duration.ofHours(1)))
                .build();

        when(repository.findAll()).thenReturn(Collections.singletonList(session));

        Iterable<FastTrackSession> sessions = service.getAllSessions();
        assertThat(sessions).containsOnly(session);
    }
}
