package org.jdriven.fasttrack.service;


import org.jdriven.fasttrack.model.FastTrackSession;
import org.jdriven.fasttrack.repository.FastTrackSessionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FastTrackServiceImplTest {

    @Mock
    private FastTrackSessionRepository repository;

    @InjectMocks
    private FastTrackSessionServiceImpl service;

    @Test
    void testGetAll() {
        FastTrackSession session = FastTrackSession.builder().id(1L).subject("Spring Boot 2").trainer("Willem Cheizoo")
                .startTime(LocalDateTime.now().minus(Duration.ofHours(1)))
                .endTime(LocalDateTime.now().plus(Duration.ofHours(1)))
                .build();

        when(repository.findAll()).thenReturn(Collections.singletonList(session));

        Iterable<FastTrackSession> sessions = service.getAllSessions();
        assertThat(sessions).containsOnly(session);

        verify(repository).findAll();
    }
}
