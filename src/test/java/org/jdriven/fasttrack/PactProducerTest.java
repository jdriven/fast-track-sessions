package org.jdriven.fasttrack;

import au.com.dius.pact.model.Interaction;
import au.com.dius.pact.model.Pact;
import au.com.dius.pact.provider.junit.IgnoreNoPactsToVerify;
import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactBroker;
import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import org.apache.http.HttpRequest;
import org.jdriven.fasttrack.model.FastTrackSession;
import org.jdriven.fasttrack.repository.FastTrackSessionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Provider("FastTrackSessionProvider")
@PactBroker(host = "localhost", port = "80")
@IgnoreNoPactsToVerify
public class PactProducerTest {

    @LocalServerPort
    private int serverPort;

    @MockBean
    private FastTrackSessionRepository fastTrackSessionRepository;

    @BeforeEach
    void setupTestTarget(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", serverPort, "/"));
    }

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void testTemplate(Pact pact, Interaction interaction, HttpRequest request, PactVerificationContext context) {
        context.verifyInteraction();
    }

    @State({"A FastTrackSession list"})
    public void toGetFastTrackSessionList() {
        FastTrackSession session = FastTrackSession.builder()
                .id(1L)
                .subject("Spring Boot 2")
                .trainer("Willem Cheizoo")
                .startTime(LocalDateTime.now().minus(Duration.ofHours(1)))
                .endTime(LocalDateTime.now().plus(Duration.ofHours(1)))
                .build();

        when(fastTrackSessionRepository.findAll()).thenReturn(Collections.emptyList());
        //when(fastTrackSessionRepository.findAll()).thenReturn(Collections.singletonList(session));
    }
}
