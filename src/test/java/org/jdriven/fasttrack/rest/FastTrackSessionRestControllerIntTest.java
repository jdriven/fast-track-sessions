package org.jdriven.fasttrack.rest;

import org.jdriven.fasttrack.model.FastTrackSession;
import org.jdriven.fasttrack.repository.FastTrackSessionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FastTrackSessionRestControllerIntTest {

    @MockBean
    private FastTrackSessionRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void testGetSession() throws Exception {
        FastTrackSession session = FastTrackSession.builder().id(1L).subject("Spring Boot 2").trainer("Willem Cheizoo")
                .startTime(LocalDateTime.now().minus(Duration.ofHours(1)))
                .endTime(LocalDateTime.now().plus(Duration.ofHours(1)))
                .build();
        when(repository.findById(1L)).thenReturn(Optional.of(session));

        ResponseEntity<FastTrackSession> sessionsResponse = restTemplate.getForEntity("/sessions/1", FastTrackSession.class);

        assertThat(sessionsResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}