package org.jdriven.fasttrack.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.SerializationUtils;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class FastTrackSessionTest {

    private FastTrackSession fastTrackSession;

    @BeforeEach
    public void createFastTrackSession() {
        fastTrackSession = FastTrackSession.builder().id(1L).subject("Spring Boot 2").trainer("Willem Cheizoo")
                .startTime(LocalDateTime.now().minus(Duration.ofHours(1)))
                .endTime(LocalDateTime.now().plus(Duration.ofHours(1)))
                .build();
    }

    @Test
    public void testSerialization() {
        FastTrackSession other = (FastTrackSession) SerializationUtils.deserialize(SerializationUtils.serialize(fastTrackSession));
        assertThat(other.getId()).isEqualTo(fastTrackSession.getId());
        assertThat(other.getSubject()).isEqualTo(fastTrackSession.getSubject());
        assertThat(other.getTrainer()).isEqualTo(fastTrackSession.getTrainer());
        assertThat(other.getStartTime()).isEqualTo(fastTrackSession.getStartTime());
        assertThat(other.getEndTime()).isEqualTo(fastTrackSession.getEndTime());
    }
}